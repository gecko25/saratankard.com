import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route } from 'react-router-dom';
import About from './components/About';
import Blog from './components/Blog';
import SampleWork from './components/SampleWork';
import Home from './components/Home';
import Picks from './components/Picks';
import BlogData from './components/BlogData';

const year = new Date().getFullYear();

class App extends React.Component {
    constructor() {
      super();
      this.select = this.select.bind(this)
      this.toggleMobileMenu = this.toggleMobileMenu.bind(this)
      this.state = {
          selectedNav: 'home',
          showMobileMenu: false
      }
    }
    select(route){
        this.toggleMobileMenu();
        this.setState({
            selectedNav: route
        })
    }

    toggleMobileMenu(){
        const that = this;
        this.setState({
             showMobileMenu: !that.state.showMobileMenu
        })
    }

    render(){

        return(
            <Router>
              <div>
                  <nav id="navigation">
                    <div className="desktop">
                        <a className={`option ${this.state.selectedNav === 'home' ? 'selected' : ''}`} onClick={this.select.bind(this, 'home')} href="#/">Home</a>
                        <a className={`option ${this.state.selectedNav === 'about' ? 'selected' : ''}`} onClick={this.select.bind(this, 'about')} href="#/about">About</a>
                        <a className={`option ${this.state.selectedNav === 'blog' ? 'selected' : ''}`} onClick={this.select.bind(this, 'blog')} href="#/blog">Blog</a>
                        <a className={`option ${this.state.selectedNav === 'work' ? 'selected' : ''}`} onClick={this.select.bind(this, 'work')} href="#/samplework">Sample Work</a>
                        <a className={`option cool-things ${this.state.selectedNav === 'picks' ? 'selected' : ''}`} onClick={this.select.bind(this, 'picks')} href="#/picks">Stuff I think is cool</a>
                    </div>

                    <div className="mobile">
                        <div className="menu-icon-container" onClick={this.toggleMobileMenu}><i className="material-icons menu-icon">reorder</i></div>
                        <div className= {`drop-down-menu-container ${this.state.showMobileMenu ? '' : 'hidden'}`}>
                            <a className={`option ${this.state.selectedNav === 'home' ? 'selected' : ''}`} onClick={this.select.bind(this, 'home')} href="#/">Home</a>
                            <a className={`option ${this.state.selectedNav === 'about' ? 'selected' : ''}`} onClick={this.select.bind(this, 'about')} href="#/about">About</a>
                            <a className={`option ${this.state.selectedNav === 'blog' ? 'selected' : ''}`} onClick={this.select.bind(this, 'blog')} href="#/blog">Blog</a>
                            <a className={`option ${this.state.selectedNav === 'work' ? 'selected' : ''}`} onClick={this.select.bind(this, 'work')} href="#/samplework">Sample Work</a>
                            <a className={`option cool-things ${this.state.selectedNav === 'picks' ? 'selected' : ''}`} onClick={this.select.bind(this, 'picks')} href="#/picks">Stuff I think is cool</a>
                        </div>
                    </div>

                </nav>

                  <main>
                      <Route exact path="/" component={Home}/>
                      <Route exact path="/about" component={About}/>
                      <Route path="/blog" component={Blog}/>
                      <Route exact path="/samplework" component={SampleWork}/>
                      <Route exact path="/picks" component={Picks}/>
                  </main>

                    <footer id="footer">
                        <div className="copyright">&copy;{year} Sara Tankard</div>
                        <div className="last-updated">Last updated March 3, 2017</div>
                        <a href="http://facebook.com/saratankard">
                            <img src="images/facebook.png" className="social-icon"></img>
                        </a>

                        <a href="http://twitter.com/saratanky">
                            <img src="images/twitter.png" className="social-icon"></img>
                        </a>

                        <a href="https://github.com/gecko25">
                            <img src="images/github.png" className="social-icon"></img>
                        </a>
						
						<a href="https://bitbucket.com/gecko25">
							<img src="images/bitbucket-icon.png" className="social-icon"></img>
						</a>

                        <a href="mailto:sara.tankard@gmail.com">
                            <img src="images/email-icon.png" className="social-icon"></img>
                        </a>

                        <a href="https://www.linkedin.com/in/saratankard/">
                            <img src="images/linked-in.png" className="social-icon"></img>
                        </a>

                        <a href="/images/resume.pdf">
                            <img src="images/resume.png" className="social-icon"></img>
                        </a>

                    </footer>
              </div>
            </Router>
        )

    }
}


ReactDOM.render(<App />, document.getElementById('root'));
