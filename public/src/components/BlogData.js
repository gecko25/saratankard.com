import EventLoop from './BlogEntries/EventLoop';
import AsynchronousProcessingAndCallbacksMetaphor from './BlogEntries/AsynchronousProcessingAndCallbacksMetaphor';
import ReactiveProgrammingResources from './BlogEntries/ReactiveProgrammingResources';
import WomenWhoCodeLightningTalks from './BlogEntries/WomenWhoCodeLightningTalks'

const BlogMetaData = [
    {
        title: 'Single threaded Javascript and the event loop',
        date: 'Tues, Dec 8 2015',
        path: '/blog/single-thread-javascript-and-the-event-loop',
        component: EventLoop,
        markup: EventLoop()
    },
    {
        title: 'Reactive Programming Resources',
        date: 'Mon, Nov 30 2015',
        path: '/blog/reactive-programming-resources',
        component: ReactiveProgrammingResources,
        markup: ReactiveProgrammingResources()
    },
    {
        title: 'Asynchronous Processing and Callback functions: A metaphor',
        date: 'Weds, July 29 2015',
        path: '/blog/asynchronous-processing-and-callbacks-metaphor',
        component: AsynchronousProcessingAndCallbacksMetaphor,
        markup: AsynchronousProcessingAndCallbacksMetaphor()
    },
    {
        title: 'Women Who Code NYC Meetup -- Lightning Talks',
        date: 'Tues, July 7 2015',
        path: '/blog/women-who-code-lightning-talks-july',
        component: WomenWhoCodeLightningTalks,
        markup: WomenWhoCodeLightningTalks()
    }
]

export default BlogMetaData;
