import React from 'react';

const Picks = ({}) => {
    return (
        <section id="picks">
            <div>
                <a className="pick" href="https://frontendmasters.com/"> Front End Masters </a>
                <a className="pick" href="http://frontendhappyhour.com/"> Front End Happy Hour Podcast</a>
                <a className="pick" href="https://devchat.tv/js-jabber"> Javascript Jabber Podccast</a>
                <a className="pick" href="https://themoth.org/"> The Moth</a>
                <a className="pick" href="https://www.ted.com/talks/don_tapscott_how_the_blockchain_is_changing_money_and_business">Blockchain ideas</a>
                <a className="pick" href="http://nodebots.io/">Node bots &amp; IOT</a>

                <a className="pick" href="https://www.cityharvest.org/"> City Harvest </a>
                <a className="pick" href="http://actualfood.com/"> Actual Food </a>
                <a className="pick" href="http://michaelpollan.com/"> Books by Michael Pollan </a>
                <a className="pick" href="http://atulgawande.com/book/being-mortal/"> Being Mortal by Atul Gawande </a>
                <a className="pick" href="http://www.justfood.org/farmschoolnyc">FarmSchoolNyc</a>
                <a className="pick" href="http://katchkiefarm.com/csa/">KatchkieFarm CSA</a>
                <a className="pick" href="http://havenskitchen.com/cooking-classes.html"> Cooking Classes @ Havens Kitchen</a>
                <a className="pick" href="http://www.unioncraftbrewing.com/"> Union Brewery </a>

                <a className="pick" href="http://openbuildinginstitute.org/"> Open Building Institute </a>
                <a className="pick" href="http://whalebonemag.com/"> Whalebone Magazine </a>
                <a className="pick" href="http://www.theinertia.com/">The Inertia </a>
                <a className="pick" href="https://www.5gyres.org/"> The 5 Gyres Institure</a>
                <a className="pick" href="http://www.bbcearth.com/planetearth2/">Planet Earth 2</a>
                <a className="pick" href="http://radioambulante.org/en/">Radio Ambulante Podcast</a>
                <a className="pick" href="https://www.facebook.com/greendoorpubvt/">Green Door Pub @ Stratton Mountain</a>
                <a className="pick" href="http://coldcoldcold.cagetheelephant.com/"> Cage the Elephant </a>
                <a className="pick" href="http://bombaestereo.com/"> Bomba Estéreo </a>




            </div>

            <div>
                .....and a bunch of other stuff &#x263A;
            </div>
        </section>
    )
};

export default Picks;
