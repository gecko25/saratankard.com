import React from 'react';

class About extends React.Component{
    constructor(props) {
      super(props);
      this.state = {
          prevView: 3,
          currentView: 1,
          nextView: 2,

      }

      this.LAST_SLIDE = 3;
      this.FIRST_SLIDE = 1;
      this.decrementView = this.decrementView.bind(this)
      this.incrementView = this.incrementView.bind(this)
   }

   decrementView(){
       var that = this;
       if (this.state.currentView === this.FIRST_SLIDE){
           this.setState({
               currentView: that.LAST_SLIDE,
               nextView: that.FIRST_SLIDE,
               prevView: that.LAST_SLIDE - 1
           })
       }else if (this.state.currentView - 2 === 0){
           this.setState({
               currentView: that.FIRST_SLIDE,
               nextView: that.FIRST_SLIDE + 1,
               prevView: that.LAST_SLIDE,
           })
       }else{
           this.setState({
               currentView: that.state.currentView - 1,
               nextView: that.state.currentView,
               prevView: that.state.currentView - 2
           })
       }
   }

   incrementView(){
       var that = this;
       if (this.state.currentView === this.LAST_SLIDE){
           this.setState({
               currentView: that.FIRST_SLIDE,
               nextView: that.FIRST_SLIDE + 1,
               prevView: that.LAST_SLIDE
           })
       }else if(this.state.currentView + 2 > this.LAST_SLIDE){
           this.setState({
               currentView: that.LAST_SLIDE,
               nextView: that.FIRST_SLIDE,
               prevView: that.LAST_SLIDE -1
           })
       }else{
           this.setState({
               currentView: that.state.currentView + 1,
               nextView: that.state.currentView + 2,
               prevView: that.state.currentView,
           })
       }
   }


    render(){
        return (
            <section id="about">
                <div className={`left arrow`} onClick={this.decrementView}>
                    <i className="material-icons icon">keyboard_arrow_left</i>
                </div>

                <div className="right arrow" onClick={this.incrementView}>
                    <i className="material-icons icon">keyboard_arrow_right</i>
                </div>

                <div className={`slide one ${this.state.currentView === 1 ? 'current' : ''} ${this.state.nextView === 1 ? 'next' : ''} ${this.state.prevView === 1 ? 'previous' : ''}`}>
                    <div className="image">
                        <div className="text">
                            <p> I grew up in Maryland and went to university at Johns Hopkins.</p>
                            <p>By the time I graduated in 2012, however, I had realized too late what I really should have studied was computer science.</p>
                        </div>
                    </div>
                </div>

                <div className={`slide two ${this.state.currentView === 2 ? 'current' : ''} ${this.state.nextView === 2 ? 'next' : ''} ${this.state.prevView === 2 ? 'previous' : ''}`}>
                    <div className="image">
                        <div className="text">
                            <p> Nonetheless, I convinced a <a href= "http://www.geckointernational.com/">tech company</a> in Costa Rica to hire me as an intern.
                                I lived and worked there for a year and a half. I learned a lot about software applications in tourism, entertainment systems, and energy management.
                                In addition to learning to surf as well as speak spanish, I also gained a deep respect and love for our earth.</p>
                        </div>
                    </div>

                </div>

                <div className={`slide three ${this.state.currentView === 3 ? 'current' : ''} ${this.state.nextView === 3 ? 'next' : ''} ${this.state.prevView === 3 ? 'previous' : ''}`}>
                    <div className="image">
                        <div className="text">
                            <p>Alas, life has led me to new adventures in New York City where there is a little less earthly beauty, but a little more Javascript&#x263A;
                            I am a front-end developer here and my latest interests have been all things react. I also am interested in decentralized systems like blockchain technologies.
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default About;
