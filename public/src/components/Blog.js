import React from 'react';
import BlogData from './BlogData'

class Blog extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          currentArticle: BlogData[0]
      }
      this.changeView = this.changeView.bind(this)
      this.toggleMenu = this.toggleMenu.bind(this)
      this.closeMenu = this.toggleMenu.bind(this)
    }

    componentWillMount(){
        BlogData.forEach(blog => {
            let requestedPath = this.props.location.pathname.substring(6); /* saratankard.com/#/blog/some-name-of-article */
            if(blog.path.indexOf(requestedPath ) > 0){
                this.setState({
                    currentArticle: blog
                })
            }
        })
    }

    componentWillReceiveProps(nextProps){
        if (nextProps.location){
            BlogData.forEach(blog => {
                let requestedPath = nextProps.location.pathname.substring(6); /* saratankard.com/#/blog/some-name-of-article */
                if(blog.path.indexOf(requestedPath ) > 0){
                    this.setState({
                        currentArticle: blog
                    })
                }
            })
        }
    }
    changeView(blog){
        this.setState({
            currentArticle: blog,
            mobileMenuIsOpen: false

        })
    }
    toggleMenu(e){
        const that = this;
        this.setState({
            mobileMenuIsOpen: !that.state.mobileMenuIsOpen
        })
    }

    closeMenu(e){
        console.log(e.target)
        const that = this;
        this.setState({
            mobileMenuIsOpen: false
        })
    }

    render(){
        const that = this;
        const links = BlogData.map( (blog) => {
            return <a href={'#' + blog.path} className="article-option" onClick={that.changeView.bind(this, blog)} key={blog.path}>{blog.title}</a>
        })

        return (
            <section id="blog" className={`${this.state.mobileMenuIsOpen ? 'dim-background' : ''}`}>
                <div className="blog-navigation">
                    <div className="desktop links">{links}</div>

                    <div className="mobile links">
                         <div className="menu-icon-container">
                             <i onClick={this.toggleMenu} className="material-icons open-menu-icon">description</i>
                         </div>


                        <div className={`article-options-container ${this.state.mobileMenuIsOpen ? 'open' : 'closed'}`}>
                            <div className="article-options-wrapper">
                                <i onClick={this.toggleMenu} className="material-icons close-menu-icon">clear</i>
                                <div className="links-container">{links}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="article">
                    <a className="title" href={'#' + this.state.currentArticle.path }>{this.state.currentArticle.title}</a>
                    <div className="date">{this.state.currentArticle.date}</div>
                    <div className="markup">{this.state.currentArticle.markup}</div>
                </div>

            </section>
        )
    }

};

export default Blog;
