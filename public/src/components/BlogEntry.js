import React from 'react';

const BlogEntry = ({
    title,
    date,
    content
}) => {
    return (
        <section className="blog-entry">
            <div className="title">{title}</div>
            <div className="date">{date}</div>
            <div className="content">{content}</div>
        </section>
    )
};

export default BlogEntry;
