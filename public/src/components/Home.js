import React from 'react';

const Home = ({}) => {
    return (
        <section id="home">
            <div className="intro-container">
                <div className="name">Sara Tankard</div>
                <div className="title">Front-End Developer</div>
            </div>
        </section>
    )
};

export default Home;
