import React from 'react';

const SampleWork = ({}) => {
    return (
        <section id="sample-work">
            <div className="header">
                Some apps I have built:
            </div>

            <div className="sample-work-container">
                <div className="preview-image">

                    <a href="http://showmethemusic.co" target="_blank">
                        <img src="images/showmethemusic.gif"></img>
                    </a>
                    <a href="http://www.showmethemusic.co">showmethemusic.co</a>
                </div>
                <div className="project-description">

                    <p className="description">
                        Search for music events and make a Spotify playlist to preview acts and decide who to see.
                    </p>

                    <p className="specs">Written in React with Redux and Express. It connects information from 3 APIS: <a href="http://www.songkick.com/developer">songkick</a>,
                        <a href="https://developer.spotify.com/web-api/">spotify</a>, and <a href="http://www.last.fm/api">last.fm</a>
                    </p>

                    <p>Design credit goes to my good friend <a href="http://samanthacombs.io/"> Samantha Combs </a></p>
                </div>
            </div>

            <div className="sample-work-container">
                <div className="preview-image">
                    <a href="http://fixmelathe.herokuapp.com/" target="_blank">
                        <img src="images/fixme-preview-min.png"></img>
                    </a>
                    <a href="http://fixmelathe.herokuapp.com/">Fix Me</a>
                </div>
                <div className="project-description">
                    <p className="description">
                        Diagnose your symptoms with this AI doctor
                    </p>

                    <p className="specs"> Written in Angular 1 with <a href="https://material.angularjs.org/latest/">Angular Material Design</a>.
                        It also uses the <a href="https://developer.infermedica.com/">Infermedica</a> API.
                    </p>
                </div>
            </div>


        </section>
    )
};

export default SampleWork;
