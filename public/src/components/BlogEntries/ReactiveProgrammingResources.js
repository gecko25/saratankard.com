import React from 'react';

const ReactiveProgrammingResources = () => {
    return (
            <div className="blog-body">
                <p>After watching Netflix's Jafar Husain of Netflix give a talk on <a href="https://frontendmasters.com/courses/asynchronous-javascript">frontendmasters.com</a>, I'm completed fascinated by it. Here are some resources in this realm:</p>
                <ul className="rx-resources">
                    <li> - Jafar Husain's workshop on <a href="https://frontendmasters.com/courses/asynchronous-javascript/">frontendmasters.com</a></li>
                    <li> - Kyle Simpsons <a href="http://blog.getify.com/concurrently-javascript-1">blog series</a> on Concurrency &amp; Asynch stuff</li>
                    <li> - <a href="http://xgrommx.github.io/rx-book/"> Free online book</a> by Denis Stoyanov on RxJs library</li>
                    <li> - <a href="http://rxmarbles.com/">RxMarbles</a> by Andre Staltz</li>
                </ul>
            </div>
    )
};

export default ReactiveProgrammingResources;
