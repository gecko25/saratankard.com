import React from 'react';
import Highlight from 'react-highlight';

const EventLoop = () => {
    return (
        <div className="blog-body">
            <p>This is my understanding of asynchronous processing: </p>

            <p>Lets consider a father who wants to make some chili.
            He has already gathered all the vegetables he needs from his garden,
            but needs to obtain some spices from the grocery store.
            So he sends his son to the store to get the spices. The father
            is very busy so he also gives his son the recipe instructions for how to
            finish cooking the chili. This way once the son gets the spices,
            he knows what to do</p>

            <p>The father continues on his business to make some cornbread.</p>

            <p>This is how I understand asynchronous processing. The important part in this
            analogy is the <em> passing of instructions </em>. </p>

            <p> In real life and in computation some tasks
            are easy and quick, others are more involved and require errands, communicating
            with other people, etc. Not all tasks are created equally. </p>

            <p>The father says to his son hey, I cut up the veggies
            can you go and shop for the spices? Also I am going to
            give you the instructions for what to do once you have the spices&mdash;
            and the veggies I already prepared&mdash; so I don't have to wait. Thanks.</p>

            <p>This is similar to how the below code works:</p>
                <Highlight className="javascript">
                    {"var http = require('http');\n\n"}
                    {"http.get(url, function cbfunc(response){\n"}
                    {"  response.on('data', function(data){\n"}
                    {"  console.log(data);\n"}
                    {"});\n"}
                </Highlight>

                <p>
                his is saying:
                Hey you! http module!
                I'll prepare the url for you. Now, you go use that url to get a response for me please.
                Oh also, I don't really have time to wait around for the response....
                so here's the instructions for what to do with that response object&mdash; the function you can.. callback
                </p>

                <p>In terms of the metaphor:</p>

                <ul>
                    <li>steps father needs to complete === main thread</li>
                    <li>prepared veggies === url</li>
                    <li>son going to the store === asynchronous function=http.get(..)</li>
                    <li>spices at the store === response</li>
                    <li>recipe=callback function === cbfunction(response)</li>
                </ul>


                <p>The heartbeat of asynchronous programming says,  I'll do what I can
                first, but now I will pass this time-consuming part of this task off to you (things
                like making a server request across the network). Also,
                I am going to give you the instructions for how to fully complete the task
                once you finish doing all that stuff that takes forever. </p>

        </div>
    )
};

export default EventLoop;
