import React from 'react';

const EventLoop = () => {
    return (
            <div className="blog-body">
                <p>So what's the deal with Javascript? It only has one-thread.
                    This means that function A has to completely finish before function B can begin. Everytime. Or function A calls function B, function B executes, and only then can function A can carry-on. Never is function A happening at the same time as function B. And this can actually be helpful in managing the state of the program and writing predictable, bug-free code.
                    Check out <a href="http://blog.getify.com/concurrently-javascript-1/">Kyle Simpsons blog series on these topics</a>.
                </p>

                <p>So then how come our browsers are not slow as molasses on a cold winter day? In theory I know this answer to be: asynchronous callbacks. </p>

                <p>But here is where I used to get confused. Aren't asynchronous callbacks just a fancy new term that describes the same things as threads? If one main thread calls 3 asynchronous functions, and those 3 asynchronous functions go off and do something while my main thread is executing at the same time ....isn't this concurrency?</p>

                <p>Well, we need to be very careful here on what we are defining as single threaded. The Javascript <i>runtime</i> is single-threaded. Calls that we make to <a href="https://developer.mozilla.org/en-US/docs/Web/API">Web APIS</a> (things like the <a href="https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model">DOM</a>, ajax calls, and other methods provided to us like <a href="https://developer.mozilla.org/en-US/docs/Web/API/WindowTimers/setTimeout">setTimeout</a>) <i>are outside of the Javscript engine</i>, so they do not count. (But yes, as far as conceptualizing things happening concurrently, these Web API calls are happening concurrently. They are effectively are like threads. BUT they are, again, outside the JS Engine AND they are not like threads in other languages that you can control in any way.)</p>

                <p>So, <strong>your Javascript runtime still only does one thing at a time.</strong></p>

                <p>Next question: <strong>How are callbacks handled when the API returns a response?</strong> How does the javascript runtime get this data again? <i>The event loop and the callback queue.</i></p>

                <p>For this answer I will show you Phillip Robert's excellent graphic from his excellent talk: <a href="https://youtu.be/8aGhZQkoFbQ">What the heck is the event loop anyway?</a> at JSConf EU 2014</p>

                <a href="images/eventloop.png"><img style={{maxWidth: '100%'}} src="images/eventloop.png"></img></a>

                <p>When a callback function is received by the browser it goes in the callback queue. <strong>Only when the runtime stack is completely clear can the event loop move callback functions into the stack</strong>, where the Javascript runtime can continue processing in a single-threaded fashion.</p>

                <p>So yes, Javascript is single-threaded. But the browser is so much more than Javascript. It has Web APIs, it has a callback queue, and it has an event loop. </p>


            </div>
    )
};

export default EventLoop;
