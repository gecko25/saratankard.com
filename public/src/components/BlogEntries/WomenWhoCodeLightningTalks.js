import React from 'react';

const WomenWhoCodeLightningTalks = () => {
    return (
            <div className="blog-body">
                <p>Bienvenidos a Nueva York! I intend to keep most of my posts technical, but for my first one I want to write about the <em><a href = "http://www.meetup.com/WomenWhoCodeNYC/events/220650892/"> Women Who Code </a> </em> meetup I had the opportunity to attend yesterday. </p>

                <p>I saw 10 lovely ladies speak. It was my first event like this with all women, and I loved it.
                Of course diversity of all kinds of is necessary for great collaboration, but there is something very relaxing
                about working with women, as a woman. One of the reasons I love programming so much is because
                of the lack of discrimination and BS that, comparitively, seems to accompany other professions. </p>

                <p>In programming I often feel the premise tends to be, "Can you make it work? Great." No one cares
                who you are, what you look like, where or even if you went to college, just prove you
                have the skills and enjoy making things work. Being surrounded by action-oriented women extends the attitude of minimizing BS. It inspires creation and acceptance --even encourgement-- of failure to dominate the culture of thinking. (<a href="http://mauryacouvares.blogspot.com/">Maurya Couvares</a>, the founder of <a href="https://scripted.org/"> ScriptEd </a>, even spoke about the importance of learning how to fail in the context of her company).</p>

                <p>Speakers discussed the technical--js promises, <a href="http://es6-features.org/">ES6 features</a>, overview of Swift, <a href="http://www.w3.org/WAI">web accessibility</a> -- and nontechnical alike. </p>

                <p><strong>One thing worth noting</strong> were 2 vibrant ladies who have recently started the company <a href="http://monarq.co/">monarq</a>-- a friendship app for women. Even more interestingly, they also started a <a href = "http://shehacksnyc.monarq.co/"> women's hackathon </a> which it think is fan-freaking-tastic. I definitely intend on going to in October. </p>

                <p>Another women's oriented world-wide collaboritive event that was discussed was <a href="http://womenseday.org"> Womens Entrepreneurship Day </a>.</p>

            </div>
    )
};

export default WomenWhoCodeLightningTalks;
